// @flow

declare class Window extends Window {
  crypto: Crypto;
  msCrypto: Crypto;
  webkitCrypto: Crypto;
}

declare var window: Window;
