# Cryptology

> cryptology (krĭp-tŏlˈə-jē)  
> n.  The study of cryptanalysis or cryptography.

---

**Disclaimer:** All the algorithms in this repository are implemented with
educational purposes in mind, sometimes implementing unnecessary functions for
the sole purpose of learning.  
While the present implementations try to be compatible with current
implementations in the wild, by no mean they should be considered secure or
optimized. 

**DO NOT USE ANY CODE OF THIS REPOSITORY IN PRODUCTION SYSTEMS** 

---

## Implemented Ciphers

- [x] [Caesar's Cipher](https://gitlab.com/couto/cryptology/tree/master/packages/cryptology-caesar)
- [x] [Simple Substitution Cipher](https://gitlab.com/couto/cryptology/tree/master/packages/cryptology-simple-substitution)
