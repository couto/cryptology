# Feature Checklist

## General Checklist

- [ ] Follows code standards?
- [ ] Is free of ESLint warnings/errors?
- [ ] Has flow types?
- [ ] Has unit tests?

## Documentation Checklist

- [ ] Has a README.md file explaining the feature?
- [ ] Has link to wikipedia page?
- [ ] Has API definition?

## Implementation Checklist

- [ ] Is it compatible with other implementations? (e.g. [Cryptool Online](https://www.cryptool.org/en/cryptool-online))


