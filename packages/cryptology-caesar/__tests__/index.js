// @flow

import { encipher, decipher, createKey } from "../";

describe("Caesar's Cipher", () => {
  describe("Defaults", () => {
    const examples = {
      ABCDEFGHIJKLMNOPQRSTUVWXYZ: "DEFGHIJKLMNOPQRSTUVWXYZABC",
      "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG":
        "WKH TXLFN EURZQ IRA MXPSV RYHU WKH ODCB GRJ",
      "HELLO WORLD": "KHOOR ZRUOG",
    };

    describe("encipher", () =>
      it("should return the correct ciphertext", () =>
        Object.keys(examples).forEach(key =>
          expect(encipher(key)).toEqual(examples[key]),
        )));

    describe("decipher", () =>
      it("should return the correct plaintext", () =>
        Object.keys(examples).forEach(key =>
          expect(decipher(examples[key])).toEqual(key),
        )));
  });

  describe("Personalized Key", () => {
    describe("encipher", () => {
      it("should shift by the specified number", () => {
        expect(encipher("A", createKey(-1))).toEqual("Z");
        expect(encipher("A", createKey(-2))).toEqual("Y");
        expect(encipher("A", createKey(-13))).toEqual("N");
        expect(encipher("A", createKey(13))).toEqual("N");
        expect(encipher("A", createKey(2))).toEqual("C");
        expect(encipher("A", createKey(1))).toEqual("B");
      });

      it("should shift by the specified number in the  give direction", () => {
        expect(encipher("A", createKey(1))).toEqual("B");
        expect(encipher("A", createKey(-1))).toEqual("Z");
      });

      it("should use the given alphabet", () => {
        const key = createKey(-4, "!@#$%^&*()");
        expect(encipher("!@", key)).toEqual("&*");
      });
    });

    describe("decipher", () => {
      it("should shift by the specified number", () => {
        expect(decipher("Z", createKey(-1))).toEqual("A");
        expect(decipher("Y", createKey(-2))).toEqual("A");
        expect(decipher("N", createKey(-13))).toEqual("A");
        expect(decipher("N", createKey(13))).toEqual("A");
        expect(decipher("C", createKey(2))).toEqual("A");
        expect(decipher("B", createKey(1))).toEqual("A");
      });

      it("should shift by the specified number in the  give direction", () => {
        expect(decipher("B", createKey(1))).toEqual("A");
        expect(decipher("Z", createKey(-1))).toEqual("A");
      });

      it("should use the given alphabet", () => {
        const key = createKey(-4, "!@#$%^&*()");
        expect(decipher("&*", key)).toEqual("!@");
      });
    });
  });
});
