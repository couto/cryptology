// @flow

import { mod } from "cryptology-math";
import { map } from "cryptology-strings";

// prettier-ignore
export const ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

export type Key = {
  shift: number,
  alphabet: string,
};

export const createKey = (
  shift: number = 3,
  alphabet: string = ALPHABET,
): Key => ({ shift, alphabet });

export const encipher = (
  plaintext: string,
  { alphabet, shift }: Key = createKey(),
): string =>
  map(char => {
    const idx = alphabet.indexOf(char);
    return idx === -1 ? char : alphabet[mod(shift + idx, alphabet.length)];
  }, plaintext);

export const decipher = (
  cyphertext: string,
  { alphabet, shift }: Key = createKey(),
): string =>
  map(char => {
    const idx = alphabet.indexOf(char);
    return idx === -1 ? char : alphabet[mod(shift * -1 + idx, alphabet.length)];
  }, cyphertext);
