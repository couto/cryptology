# [Caesar Cipher](https://en.wikipedia.org/wiki/Caesar_cipher)

---

## Examples

### Original Caesar's Cipher

```JavaScript
import { encipher, decipher } from 'cryptology-caesar-cipher';

const original = 'HELLO WORLD';
const cyphertext = encipher(original); // EBIIL TLOIA
const plaintext = decipher(cyphertext); // HELLO WORLD
```

### ROT13

```JavaScript
import { createKey, encipher, decipher } from 'cryptology-caesar-cipher';

const plaintext = 'HELLO WORLD';
const key = createKey(13);
const cyphertext = encipher(original, key); // URYYB JBEYQ
const plaintext = decipher(cyphertext, key); // HELLO WORLD
```

### Extra personalizations

```JavaScript
import { createKey, encipher, decipher } from 'cryptology-caesar-cipher';

const plaintext = '#$!@';
const key = createKey(13, "!@#$%^&");
const cyphertext = encipher(original, key); // @#&!
const plaintext = decipher(cyphertext, key); // #$!@
```
