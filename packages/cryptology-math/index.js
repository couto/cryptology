// @flow

// @see https://math.stackexchange.com/questions/519845/modulo-of-a-negative-number
// eslint-disable-next-line
export const mod = (n: number, m: number): number => ((n % m) + m) % m;

