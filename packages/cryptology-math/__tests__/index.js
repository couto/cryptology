// @flow

import { mod } from "../";

describe("modulo", () => {
  it("should return 4", () => expect(mod(-100, 8)).toEqual(4));
  it("should return 3", () => expect(mod(-17, 5)).toEqual(3));
  it("should return 0", () => expect(mod(4, 4)).toEqual(0));
});
