// @flow

/**
 * DISCLAIMER
 * Please open an issue if any of the following alphabets are incorrect.
 */

// @see https://en.wikipedia.org/wiki/ISO_basic_Latin_alphabet
export const LATIN = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

// @see https://en.wikipedia.org/wiki/Greek_alphabet
export const GREEK = "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ";

// @see https://en.wikipedia.org/wiki/Cyrillic_script
export const CYRILLIC = "АБВГҐДЂЃЕЁЄЖЗЗ́ЅИІЇЙЈКЛЉМНЊОПРСС́ТЋЌУЎФХЦЧЏШЩЪЫЬЭЮЯ";
