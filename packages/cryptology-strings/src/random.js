// @flow

const cryptoObj: false | Crypto =
  typeof window === "object" &&
  (window.crypto || window.webkitCrypto || window.msCrypto);

const nodeRandomBytes = (len: number): Buffer => {
  // eslint-disable-next-line global-require
  const crypto = require("crypto");
  return crypto.randomBytes(len);
};

const cutMyLifeIntoPiecesThisIsMyLastResort = (len: number): number[] => {
  // eslint-disable-next-line no-console
  console.warn(
    `Could not find crypto.getRandomValues. Falling back to Math.random which is NOT SECURE!`,
  );
  return Array.from(Array(len).keys()).map(() => Math.round(Math.random()));
};

const browserRandomBytes = (len: number): Uint32Array | number[] =>
  cryptoObj
    ? cryptoObj.getRandomValues(new Uint32Array(len))
    : cutMyLifeIntoPiecesThisIsMyLastResort(len);

const generateRandomBytes = (len: number): Buffer | Uint32Array | number[] =>
  cryptoObj ? browserRandomBytes(len) : nodeRandomBytes(len);

export default (len: number = 26) => generateRandomBytes(len);
