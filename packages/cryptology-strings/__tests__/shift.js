// @flow

import { shift } from "../";

describe("shift", () => {
  it("should shift N letters to the right", () => {
    const original = "Hello World";
    expect(shift(3, original)).toEqual("lo WorldHel");
  });

  it("should shift N letters to the left", () => {
    const original = "Hello World";
    expect(shift(-3, original)).toEqual("rldHello Wo");
  });
});
