// @flow

import { filter } from "../";

describe.only("filter", () => {
  it("should return the given string", () => {
    const original = "Hello World";
    const identity = i => !!i;
    expect(filter(identity, original)).toEqual(original);
  });

  it("should return the result of the sum of each fn call", () => {
    const original = "Hello World";
    const removeO = i => i !== "o";
    expect(filter(removeO, original)).toEqual("Hell Wrld");
  });

  it("should call mock function with correct signature", () => {
    const original = "Hello";
    const identity = i => !!i;
    const mockFn = jest.fn(identity);

    expect(filter(mockFn, original)).toEqual(original);
    expect(mockFn).toHaveBeenCalledTimes(5);
    expect(mockFn.mock.calls).toEqual([["H"], ["e"], ["l"], ["l"], ["o"]]);
  });
});
