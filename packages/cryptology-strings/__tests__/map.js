// @flow

import { map } from "../";

describe.only("map", () => {
  it("should return the given string", () => {
    const original = "Hello World";
    const identity = i => i;
    expect(map(identity, original)).toEqual(original);
  });

  it("should return the result of the sum of each fn call", () => {
    const original = "Hello World";
    const toUpperCase = i => i.toUpperCase();
    expect(map(toUpperCase, original)).toEqual(original.toUpperCase());
  });

  it("should call mock function with correct signature", () => {
    const original = "Hello";
    const identity = i => i;
    const mockFn = jest.fn(identity);

    expect(map(mockFn, original)).toEqual(original);
    expect(mockFn).toHaveBeenCalledTimes(5);
    expect(mockFn.mock.calls).toEqual([
      ["H", 0],
      ["e", 1],
      ["l", 2],
      ["l", 3],
      ["o", 4],
    ]);
  });
});
