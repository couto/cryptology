// @flow

import { split } from "../";

describe("split", () => {
  it("should use space as default sepator", () =>
    expect(split(3, "Hello World")).toEqual("Hel lo  Wor ld"));

  it("should use the given separator", () =>
    expect(split(3, "Hello World", "$")).toEqual("Hel$lo $Wor$ld"));

  it("should split in blocks of given number", () => {
    expect(split(2, "Hello World")).toEqual("He ll o  Wo rl d");
    expect(split(4, "Hello World")).toEqual("Hell o Wo rld");
    expect(split(5, "Hello World")).toEqual("Hello  Worl d");
  });
});
