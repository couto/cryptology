// @flow

import { stripWhitespace } from "../";

describe("stripWhitespace", () => {
  it("should remove all whitespace from string", () =>
    expect(stripWhitespace("Hello Brave New World")).toEqual(
      "HelloBraveNewWorld",
    ));

  it("should remove tabs from string", () =>
    expect(stripWhitespace("Hello	Brave	New	World")).toEqual(
      "HelloBraveNewWorld",
    ));
});
