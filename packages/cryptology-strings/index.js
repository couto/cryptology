// @flow

import { mod } from "cryptology-math";

export { LATIN, GREEK, CYRILLIC } from "./src/alphabet";

/**
 * Like Array#map but for strings
 *
 * @note Flowtype doesn't allow me to piggyback on Array#map
 * @example map(char => char.toUpperCase(), "hello world") // "HELLO WORLD"
 */
export const map = (
  fn: (char: string, index: number) => string,
  str: string,
  counter: number = 0,
): string =>
  !str.length
    ? str
    : `${fn(str[0], counter)}${map(fn, str.slice(1), counter + 1)}`;

/**
 * Like Array#reduce but for strings
 * @example fold(char => console.log(char), "Hello World") // undefined
 */
export const fold = <I>(
  fn: (acc: string, char: string, index: number, str: I) => string,
  str: I,
  initialState: string = "",
): string => Array.prototype.reduce.call(str, fn, initialState);

/**
 * Like Array#filter but for strings
 * @note Flowtype doesn't allow me to piggyback on Array#map
 * @example filter(c => c !== 'a', 'AaaAaaA') // AAA
 */
export const filter = (fn: (str: string) => boolean, str: string): string =>
  !str.length ? str : `${fn(str[0]) ? str[0] : ""}${filter(fn, str.slice(1))}`;

/**
 * Shift N places on the string
 * @example shift(3, ABC) // CBA
 */
export const shift = (places: number, str: string): string =>
  map((char, idx) => str[mod(idx + places, str.length)], str);

/**
 * Remove all whitespace characters
 * @example stripWhitespace("Hello World") // "HelloWorld"
 */
export const stripWhitespace = (str: string): string => str.replace(/\s+/g, "");

/**
 * Split the given string N blocks with the given separator
 * @example split(3, "HelloWorld", "|") // Hel|loW|orl|d
 */
export const split = (
  blocks: number,
  str: string,
  separator: string = " ",
): string =>
  fold(
    (acc: string, c: string, i: number) =>
      i && !mod(i, blocks) ? acc.concat(separator, c) : acc.concat(c),
    str,
  );
