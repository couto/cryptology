// @flow

import { CYRILLIC } from "cryptology-strings";
import { encipher, decipher, createKey } from "../";

describe("Simple substitution cipher", () => {
  describe("with key 'ZEBRAS'", () => {
    const key = createKey("ZEBRAS");
    const examples = {
      "FLEE AT ONCE. WE ARE DISCOVERED!": "SIAA ZQ LKBA. VA ZOA RFPBLUAOAR!",
      "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG":
        "QDA NTFBH EOLVK SLW GTJMP LUAO QDA IZYX RLC",
    };

    it("should convert to the ciphertext correctly", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(encipher(plaintext, key)).toEqual(examples[plaintext]),
      ));

    it("should convert to the plaintext correctly", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(decipher(examples[plaintext], key)).toEqual(plaintext),
      ));
  });

  describe("Different key", () => {
    const key = createKey("1234567890?@#$%^&*()_+-=[]");
    const examples = {
      "FLEE AT ONCE. WE ARE DISCOVERED!": "6@55 1) %$35. -5 1*5 49(3%+5*54!",
      "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG":
        ")85 &_93? 2*%-$ 6%= 0_#^( %+5* )85 @1][ 4%7",
    };

    it("should convert to correct cyphertext", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(encipher(plaintext, key)).toEqual(examples[plaintext]),
      ));

    it("should convert to correct plaintext", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(decipher(examples[plaintext], key)).toEqual(plaintext),
      ));
  });

  describe("Different alphabet", () => {
    const key = createKey("1234567890?@#$%^&*()_+-=[]", CYRILLIC);
    const examples = {
      "ДЖҐҐ АЙ ́ЗВҐ. ЛҐ АІҐ ГЕЇВ́КҐІҐГ!": "6@55 1) %#35. -5 1*5 49(3%+5*54!",
      "ЙЃҐ ИЈЕВЄ БІ́ЛЗ Д́Љ ЁЈЗЅЇ ́КҐІ ЙЃҐ ЖАНМ ЃЂ":
        ")85 &_93? 2*%-# 6%= 0_#^( %+5* )85 @1][ 4%7",
    };

    it("should convert to correct cyphertext", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(encipher(plaintext, key)).toEqual(examples[plaintext]),
      ));

    it("should convert to correct plaintext", () =>
      Object.keys(examples).forEach(plaintext =>
        expect(decipher(examples[plaintext], key)).toEqual(plaintext),
      ));
  });
});
