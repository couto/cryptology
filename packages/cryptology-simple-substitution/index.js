// @flow
import { fold, filter, LATIN as ALPHABET } from "cryptology-strings";

export type Key = {
  key: string,
  alphabet: string,
};

const switchChar = (char: string, alphabet: string, key: string): string =>
  key[alphabet.indexOf(char)] || char;

export const createKey = (key: string, alphabet: string = ALPHABET): Key => ({
  key: key.concat(filter(char => !key.includes(char), alphabet)),
  alphabet,
});

export const encipher = (plaintext: string, { alphabet, key }: Key): string =>
  fold((acc, char) => acc.concat(switchChar(char, alphabet, key)), plaintext);

export const decipher = (cyphertext: string, { alphabet, key }: Key): string =>
  fold((acc, char) => acc.concat(switchChar(char, key, alphabet)), cyphertext);
