# [Simple Substitution Cipher](https://en.wikipedia.org/wiki/Substitution_cipher)

---
## Description

**Monoalphabetic Substitution**

Replace each character in the plaintext with another character. A typical
example of this is the [Caesar's Cipher](../cryptology-caesar).

The key must have, at least, the same number of characters as the original
alphabet. To keep keys small and simple, the `createKey` function will
complement the given key with the remaining letters of the English alphabet.

**Disclaimer:** It's possible to have letter clashes.  
Unless you specify your on alphabet, the original alphabet is:  
`ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`. 
If you simple add a key like: `ZEBRAS` then the resulting key is:  
`ZEBRASCDFGHIJKLMNOPQTUVWXYabcdefghijklmnopqrstuvwxyz`

If we compare the alphabet and the key:

```
ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
ZEBRASCDFGHIJKLMNOPQTUVWXYabcdefghijklmnopqrstuvwxyz
```
We can see the entire range of lower case letters match, which means that a
plain text like: "hello world" will generate the cypher text: "hello world".


## Examples

### Simple Substitution

```JavaScript
import { encipher, decipher, createKey } from 'cryptology-simple-substitution';

const key = createKey("ZEBRAS");
const original = "FLEE AT ONCE. WE ARE DISCOVERED!";
const cyphertext = encipher(original, key); // "SIAA ZQ LKBA. VA ZOA RFPBLUAOAR!"
const plaintext = decipher(cyphertext, key); // "FLEE AT ONCE. WE ARE DISCOVERED!"
```

### Different alphabet

```JavaScript
import { encipher, decipher, createKey } from 'cryptology-simple-substitution';

const key = createKey("1234567890?@#$%^&*()_+-=[]");
const original = "FLEE AT ONCE. WE ARE DISCOVERED!";
const cyphertext = encipher(original, key); // ")85 &_93? 2*%-$ 6%= 0_#^( %+5* )85 @1][ 4%7"
const plaintext = decipher(cyphertext, key); // "FLEE AT ONCE. WE ARE DISCOVERED!"
```


