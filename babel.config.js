module.exports = api => {
  const env = api.env();

  const envOpts = {
    modules: env === "test" ? "commonjs" : false,
    targets: {
      node: "current"
    }
  };

  // api.cache(false);

  return {
    presets: [["@babel/preset-env", envOpts], "@babel/preset-flow"]
  };
};
